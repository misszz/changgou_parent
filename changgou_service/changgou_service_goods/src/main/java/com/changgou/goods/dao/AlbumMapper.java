package com.changgou.goods.dao;

import com.changgou.goods.pojo.Album;
import tk.mybatis.mapper.common.Mapper;

/**
 * @author zhongxuwang
 * @since 2:19 下午
 */
public interface AlbumMapper extends Mapper<Album> {

}
