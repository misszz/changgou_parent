package com.changgou.goods.service.impl;

import com.changgou.goods.dao.BrandMapper;
import com.changgou.goods.pojo.Brand;
import com.changgou.goods.service.BrandService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;

/**
 * @author zhongxuwang
 * @since 10:41 下午
 */
@Service
public class BrandServiceImpl implements BrandService {
    @Autowired
    private BrandMapper brandMapper;

    /**
     * 条件+分页查询
     * @param page
     * @param size
     * @param brand
     * @return
     */
    @Override
    public PageInfo<Brand> findPage(Integer page, Integer size, Brand brand) {
        PageHelper.startPage(page, size);
        Example example = createExample(brand);
        return new PageInfo<>(brandMapper.selectByExample(example));
    }

    /**
     * 条件查询
     * @param brand
     */
    public List<Brand> search(Brand brand){
        //构造条件
        Example example = createExample(brand);
        List<Brand> brands = brandMapper.selectByExample(example);
        return brands;
    }

    /**
     * 多条件查询
     * @param brand
     * @return
     */
    private Example createExample(Brand brand) {
        //创建条件查询
        Example example = new Example(Brand.class);
        Example.Criteria criteria = example.createCriteria();
        if (brand != null){
            if (!StringUtils.isEmpty(brand.getName())){
                criteria.andLike("name","%" + brand.getName() + "%");
            }
            if (!StringUtils.isEmpty(brand.getLetter())){
                criteria.andEqualTo("letter",brand.getLetter());
            }
        }
        return example;
    }

    /**
     * 查询所有品牌
     * @return
     */
    @Override
    public List<Brand> findList() {
        return brandMapper.selectAll();
    }

    @Override
    public Brand findById(Integer id) {
        return brandMapper.selectByPrimaryKey(id);
    }

    @Override
    public void add(Brand brand) {
        //insertSelective 方法和insert区别：如果brand对象中属性为空 则不会拼接
        brandMapper.insertSelective(brand);
    }

    @Override
    public void updateById(Brand brand) {
        brandMapper.updateByPrimaryKeySelective(brand);
    }

    @Override
    public void delete(Integer id) {
        brandMapper.deleteByPrimaryKey(id);
    }


}
