package com.changgou.goods.controller;

import com.changgou.common.pojo.Result;
import com.changgou.common.pojo.StatusCode;
import com.changgou.goods.pojo.Brand;
import com.changgou.goods.service.BrandService;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author zhongxuwang
 * @since 11:16 下午
 */
@RestController
@RequestMapping("/brand")
public class BrandController {
    @Autowired
    private BrandService brandService;

    /**
     * 条件+分页查询
     */
    @PostMapping("/search/{page}/{size}")
    public Result<PageInfo<Brand>> findPage(@RequestBody(required = false) Brand brand, @PathVariable int page, @PathVariable int size){
        PageInfo<Brand> brandPageInfo = brandService.findPage(page, size, brand);
        return new Result<>(true, StatusCode.OK, "查询成功",brandPageInfo);
    }


    /**
     * 条件查询
     */
    @PostMapping("/search")
    public Result<List<Brand>> search(@RequestBody Brand brand){
        List<Brand> brands = brandService.search(brand);
        return new Result<>(true, StatusCode.OK,"查询成功",brands);
    }

    /**
     *  查询所有品牌
     */
    @GetMapping
    public Result<List<Brand>> findList(){
        List<Brand> brands = brandService.findList();
        return new Result<>(true, StatusCode.OK,"查询成功",brands);
    }

    /**
     * 根据id查询品牌
     */
    @GetMapping("/{id}")
    public Result<Brand> get(@PathVariable(name = "id") Integer id){
        Brand brand = brandService.findById(id);
        return new Result<>(true,StatusCode.OK,"查询成功",brand);
    }


    /**
     * 添加品牌
     * @param brand
     */
    @PostMapping
    public Result add(@RequestBody Brand brand){
        brandService.add(brand);
        return new Result<>(true,StatusCode.OK,"添加成功");
    }

    /**
     * 根据id修改品牌
     */
    @PutMapping("{id}")
    public Result updateById(@PathVariable(name = "id") Integer id,@RequestBody Brand brand){
        brand.setId(id);
        brandService.updateById(brand);
        return new Result<>(true,StatusCode.OK,"修改成功");
    }

    /**
     * 删除品牌
     */
    @DeleteMapping("/{id}")
    public Result delete(@PathVariable(name = "id") Integer id){
        brandService.delete(id);
        return new Result<>(true,StatusCode.OK,"删除成功");
    }
}
