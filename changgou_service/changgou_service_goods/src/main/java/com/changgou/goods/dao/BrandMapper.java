package com.changgou.goods.dao;

import com.changgou.goods.pojo.Brand;
import tk.mybatis.mapper.common.Mapper;

/**
 * @author zhongxuwang
 * @since 10:35 下午
 */
public interface BrandMapper extends Mapper<Brand> {

}
