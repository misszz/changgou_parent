package com.changgou.goods.dao;

import com.changgou.goods.pojo.Para;
import tk.mybatis.mapper.common.Mapper;

/**
 * @author zhongxuwang
 * @since 2:54 下午
 */
public interface ParaMapper extends Mapper<Para> {
}
