package com.changgou.goods.service;

import com.changgou.goods.pojo.Brand;
import com.github.pagehelper.PageInfo;

import java.util.List;

/**
 * @author zhongxuwang
 * @since 10:36 下午
 */
public interface BrandService {

    /**
     * 条件+分页查询
     */
    PageInfo<Brand> findPage(Integer page, Integer size, Brand brand);
    /**
     * 条件查询
     */
    List<Brand> search(Brand brand);
    /**
     * 查询所有品牌
     */
    List<Brand> findList();

    /**
     * 根据ID查询品牌
     */
    Brand findById(Integer id);

    /**
     * 新增品牌
     */
    void add(Brand brand);

    /**
     * 根据id修改品牌
     */
    void updateById(Brand brand);


    /**
     * 删除品牌
     */
    void delete(Integer id);
}
