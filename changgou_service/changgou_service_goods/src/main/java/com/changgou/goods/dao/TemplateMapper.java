package com.changgou.goods.dao;

import com.changgou.goods.pojo.Template;
import tk.mybatis.mapper.common.Mapper;

/**
 * @author zhongxuwang
 * @since 2:44 下午
 */
public interface TemplateMapper extends Mapper<Template> {
}
