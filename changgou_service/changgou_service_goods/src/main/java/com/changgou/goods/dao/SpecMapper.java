package com.changgou.goods.dao;

import com.changgou.goods.pojo.Spec;
import tk.mybatis.mapper.common.Mapper;

/**
 * @author zhongxuwang
 * @since 2:50 下午
 */
public interface SpecMapper extends Mapper<Spec> {
}
